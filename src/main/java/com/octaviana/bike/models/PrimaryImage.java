package com.octaviana.bike.models;

import org.apache.tomcat.util.codec.binary.Base64;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;

@Entity
public class PrimaryImage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "image", length = 1000)
    private byte[] image;


    public PrimaryImage() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
