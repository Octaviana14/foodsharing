package com.octaviana.bike.models;

import javax.persistence.*;

@Entity
@Table(name="login_tokens")
public class Token {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String token;
}
