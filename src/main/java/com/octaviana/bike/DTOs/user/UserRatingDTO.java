package com.octaviana.bike.DTOs.user;

public class UserRatingDTO {
    public Long userId;
    public Float rating;
}
