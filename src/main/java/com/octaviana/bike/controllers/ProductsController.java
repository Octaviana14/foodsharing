package com.octaviana.bike.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.octaviana.bike.DTOs.ProductAddInputDTO;
import com.octaviana.bike.DTOs.ProductEditInputDTO;
import com.octaviana.bike.DTOs.ProductFirstPageDTO;
import com.octaviana.bike.DTOs.ProductSingleDTO;
import com.octaviana.bike.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ProductsController {
    @Autowired
    private ProductService productService;

    @PostMapping(value = "/add-product", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE})
    public void addProduct(@RequestPart("imageFile") MultipartFile file, @RequestPart("formData") String input) throws IOException {
        productService.addProduct(file, input);
    }

    @GetMapping("/get-products/{city}")
    public List<ProductFirstPageDTO> getProductsForCity(@PathVariable String city){
        return productService.getAllByCity(city);
    }

    @GetMapping("/get-single-product/{id}")
    public ProductSingleDTO getSingleProduct(@PathVariable Long id) {
        return productService.getSingleProduct(id);
    }

    @PostMapping("/edit-product")
    public ProductSingleDTO editProduct(@RequestBody ProductEditInputDTO input) {
       return productService.editProduct(input);
    }

    @GetMapping("/get-search-options")
    public List<String> getSearchOptions(){
        return productService.getProductsForSearch();

    }

}
