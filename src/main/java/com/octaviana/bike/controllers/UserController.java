package com.octaviana.bike.controllers;

import com.octaviana.bike.DTOs.UserLoginDTO;
import com.octaviana.bike.DTOs.user.UserDetailsDTO;
import com.octaviana.bike.DTOs.user.UserEditInputDTO;
import com.octaviana.bike.DTOs.user.UserSignUpDetailsDTO;
import com.octaviana.bike.DTOs.user.UserRatingDTO;
import com.octaviana.bike.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user/")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("get-user-details/id")
    public UserDetailsDTO getUserDetails(@PathVariable Long id) {
        return userService.getUserDetails(id);
    }

    @PostMapping("edit-user")
    public void editUserDetails(@RequestBody UserEditInputDTO input) {
        userService.editUserDetails(input);
    }

    @PostMapping("sign-up")
    public Long createUser(@RequestBody UserSignUpDetailsDTO userSignUpDetailsDTO) {
        return userService.createUser(userSignUpDetailsDTO);
    }

    @PostMapping("login")
    public String login(@RequestBody UserLoginDTO userLoginDTO) {
        return userService.login(userLoginDTO);
    }

    @PostMapping("add-rating")
    public void addRating(@RequestBody UserRatingDTO userRatingDTO) {
        userService.addRating(userRatingDTO);
    }
}
