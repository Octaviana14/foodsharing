package com.octaviana.bike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodShareApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodShareApplication.class, args);
	}

}
