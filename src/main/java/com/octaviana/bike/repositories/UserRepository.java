package com.octaviana.bike.repositories;

import com.octaviana.bike.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User getOneById(Long id);
    User getOneByEmail(String email);
}
