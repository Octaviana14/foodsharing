package com.octaviana.bike.repositories;

import com.octaviana.bike.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> getAllByCityAndAndAvailableIsTrue(String city);

    Optional<Product> getOneById(Long id);

    @Query("SELECT DISTINCT p.name FROM Product p")
    List<String> findDistinctProducts();

}
