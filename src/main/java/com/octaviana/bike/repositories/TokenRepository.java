package com.octaviana.bike.repositories;


import com.octaviana.bike.models.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {

    void save(String newToken);
}
