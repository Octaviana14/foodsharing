package com.octaviana.bike.repositories;

import com.octaviana.bike.models.PrimaryImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrimaryImageRepository extends JpaRepository<PrimaryImage, Long> {
}
