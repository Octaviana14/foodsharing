package com.octaviana.bike.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.octaviana.bike.DTOs.ProductAddInputDTO;
import com.octaviana.bike.DTOs.ProductEditInputDTO;
import com.octaviana.bike.DTOs.ProductFirstPageDTO;
import com.octaviana.bike.DTOs.ProductSingleDTO;
import com.octaviana.bike.models.PrimaryImage;
import com.octaviana.bike.models.Product;
import com.octaviana.bike.models.User;
import com.octaviana.bike.repositories.PrimaryImageRepository;
import com.octaviana.bike.repositories.ProductRepository;
import com.octaviana.bike.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PrimaryImageRepository primaryImageRepository;

    @Override
    public void addProduct(MultipartFile file, String input) throws IOException {
        Product newProduct = new Product();
        ProductAddInputDTO productAddInputDTO = new ProductAddInputDTO();
        ObjectMapper objectMapper = new ObjectMapper();
        productAddInputDTO = objectMapper.readValue(input, ProductAddInputDTO.class);
        newProduct.setAvailable(true);
        newProduct.setAvailableUntil(productAddInputDTO.getAvailableUntil());
        newProduct.setCity(productAddInputDTO.getCity());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
        newProduct.setDatePosted(LocalDate.now().format(formatter));
        newProduct.setDescription(productAddInputDTO.getDescription());
        newProduct.setName(productAddInputDTO.getName());
        newProduct.setPickupLocation(productAddInputDTO.getLocation());
        newProduct.setPickupTime(productAddInputDTO.getPickupTime());
        newProduct.setCategory(productAddInputDTO.getCategory());

        PrimaryImage primaryImage = new PrimaryImage();
        primaryImage.setImage(compressBytes(file.getBytes()));
        primaryImage = primaryImageRepository.save(primaryImage);
        newProduct.setPrimaryImage(primaryImage);
        User user = new User();
        user = userRepository.getOneById(1L);
        newProduct.setUser(user);
        productRepository.save(newProduct);
    }

    private static byte[] compressBytes(byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
        }

        return outputStream.toByteArray();
    }


    @Override
    public List<ProductFirstPageDTO> getAllByCity(String city) {
        List<Product> listOfProducts = productRepository.getAllByCityAndAndAvailableIsTrue(city);
        List<ProductFirstPageDTO> listOfProductsDTO = new ArrayList<>();
        listOfProducts.forEach(product -> {
            ProductFirstPageDTO dto = new ProductFirstPageDTO();
            dto.setId(product.getId());
            dto.setName(product.getName());
            dto.setDescription(product.getDescription());
            dto.setCity(product.getCity());
            dto.setDatePosted(product.getDatePosted());
            dto.setAvailableUntil(product.getAvailableUntil());
            dto.setAvailable(product.isAvailable());
            dto.setPickupLocation(product.getPickupLocation());
            dto.setCategory(product.getCategory());
            dto.setUserName(product.getUser().getName());
            dto.setPictureBytes(decompressBytes(product.getPrimaryImage().getImage()));
            listOfProductsDTO.add(dto);
        });
        return listOfProductsDTO;
    }


    @Override
    public ProductSingleDTO getSingleProduct(Long id) {

        Optional<Product> optionalProduct = productRepository.getOneById(id);
        if (optionalProduct.isPresent()) {
            Product product = optionalProduct.get();
            ProductSingleDTO productSingleDTO = new ProductSingleDTO();
            productSingleDTO.setId(product.getId());
            productSingleDTO.setAvailable(product.isAvailable());
            productSingleDTO.setName(product.getName());
            productSingleDTO.setDescription(product.getDescription());
            productSingleDTO.setAvailableUntil(product.getAvailableUntil());
            productSingleDTO.setCity(product.getCity());
            productSingleDTO.setPickupLocation(product.getPickupLocation());
            productSingleDTO.setDatePosted(product.getDatePosted());
            productSingleDTO.setCategory(product.getCategory());
            User user = product.getUser();
            productSingleDTO.setUserName(user.getName());
            productSingleDTO.setRatingUser(user.getRating());
            productSingleDTO.setPhoneNumber(user.getPhoneNumber());

            productSingleDTO.setPictureBytes(decompressBytes(product.getPrimaryImage().getImage()));
            return productSingleDTO;
        }
        return null;
    }

    public static byte[] decompressBytes(byte[] data) {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (DataFormatException | IOException e) {
        }
        return outputStream.toByteArray();
    }


    @Override
    public ProductSingleDTO editProduct(ProductEditInputDTO input) {
        Long id = input.getProductId();
        if (id != null) {
            Optional<Product> optionalProduct = productRepository.getOneById(id);
            Product product = optionalProduct.get();
            if (optionalProduct.isPresent()) {
                String inputDescription = input.getDescription();
                if (inputDescription != null) {
                }
            }
            //  return product;
        }
        return new ProductSingleDTO();
    }

    @Override
    public List<String> getProductsForSearch() {
        return productRepository.findDistinctProducts();
    }
}
