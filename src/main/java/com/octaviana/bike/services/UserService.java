package com.octaviana.bike.services;

import com.octaviana.bike.DTOs.UserLoginDTO;
import com.octaviana.bike.DTOs.user.UserDetailsDTO;
import com.octaviana.bike.DTOs.user.UserEditInputDTO;
import com.octaviana.bike.DTOs.user.UserSignUpDetailsDTO;
import com.octaviana.bike.DTOs.user.UserRatingDTO;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    UserDetailsDTO getUserDetails(Long id);

    void editUserDetails(UserEditInputDTO id);

    Long createUser(UserSignUpDetailsDTO userSignUpDetailsDTO);

    void addRating(UserRatingDTO userRatingDTO);

    String login(UserLoginDTO userLoginDTO);
}
