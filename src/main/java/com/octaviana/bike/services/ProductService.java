package com.octaviana.bike.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.octaviana.bike.DTOs.ProductAddInputDTO;
import com.octaviana.bike.DTOs.ProductEditInputDTO;
import com.octaviana.bike.DTOs.ProductFirstPageDTO;
import com.octaviana.bike.DTOs.ProductSingleDTO;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public interface ProductService {

    void addProduct(MultipartFile file, String input) throws IOException;

    List<ProductFirstPageDTO> getAllByCity(String city);

    ProductSingleDTO getSingleProduct(Long id);

    ProductSingleDTO editProduct(ProductEditInputDTO input);

    List<String> getProductsForSearch();
}
