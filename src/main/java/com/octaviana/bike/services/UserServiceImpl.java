package com.octaviana.bike.services;

import com.octaviana.bike.DTOs.UserLoginDTO;
import com.octaviana.bike.DTOs.user.UserDetailsDTO;
import com.octaviana.bike.DTOs.user.UserEditInputDTO;
import com.octaviana.bike.DTOs.user.UserRatingDTO;
import com.octaviana.bike.DTOs.user.UserSignUpDetailsDTO;
import com.octaviana.bike.models.User;
import com.octaviana.bike.repositories.TokenRepository;
import com.octaviana.bike.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;


@Service

public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenRepository tokenRepository;

    @Override
    public UserDetailsDTO getUserDetails(Long id) {
        UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
        User user = userRepository.getOneById(id);
        userDetailsDTO.setCity(user.getCity());
        userDetailsDTO.setAddress(user.getAddress());
        userDetailsDTO.setDateJoined(user.getDateJoined());
        userDetailsDTO.setDescription(user.getDescription());
        userDetailsDTO.setEmail(user.getEmail());
        userDetailsDTO.setPhoneNumber(user.getPhoneNumber());
        return userDetailsDTO;
    }

    @Override
    public void editUserDetails(UserEditInputDTO input) {
        User user = userRepository.getOneById(input.getId());
        user.setCity(input.getCity());
        user.setAddress(input.getAddress());
        user.setDateJoined(input.getDateJoined());
        user.setDescription(input.getDescription());
        user.setEmail(input.getEmail());
        user.setPhoneNumber(input.getPhoneNumber());
    }

    @Override
    public Long createUser(UserSignUpDetailsDTO input) {
        User user = new User();
        user.setCity(input.getCity());
        user.setAddress(input.getAddress());
        user.setDateJoined(LocalDate.now().toString());
        user.setEmail(input.getEmail());
        user.setPhoneNumber(input.getPhone());
        user.setName(input.getName());
        user = userRepository.save(user);
        return user.getId();
    }

    @Override
    public void addRating(UserRatingDTO userRatingDTO) {
        User user = userRepository.getOneById(userRatingDTO.userId);
        Float ratingOld = user.getRating();
        if(ratingOld<1){
            ratingOld = userRatingDTO.rating;
        } else{
            ratingOld = (ratingOld + userRatingDTO.rating)/2;
        }
        user.setRating(ratingOld);
        userRepository.save(user);
    }

    @Override
    public String login(UserLoginDTO userLoginDTO) {
        User user = userRepository.getOneByEmail(userLoginDTO.email.toLowerCase());
        if(user.getPassword().equals(userLoginDTO.getPassword())){

            tokenRepository.save(getNewToken(user.getEmail()));
            //return token;
        }
        return null;
    }

    private String getNewToken(String email) {
        String tokenPrepare = email + LocalDateTime.now();
        return tokenPrepare;
    }
}

